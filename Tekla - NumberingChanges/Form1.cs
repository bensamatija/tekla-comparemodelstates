﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.Runtime.Remoting;
using Tekla.Structures.Internal;
using System.Diagnostics;

using System.Runtime.Serialization;
using System.Xml.Serialization;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using System.IO;
//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

using System.Collections;

namespace NumberingChanges
{
    public partial class Form1 : Form
    {
        string appName = "Tekla - Compare Model States";
        // Public (TEMPORARY):
        public List<Hashtable> objList = new List<Hashtable>();
        public List<Hashtable> slot1List = new List<Hashtable>();
        public List<Hashtable> slot2List = new List<Hashtable>();
        string userDesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        string state = "state";
        TSM.Model model = new TSM.Model();
        ArrayList strProps = new ArrayList() { "DR_PART_POS", "PART_POS", "ASSEMBLY_POS", "GUID", "OBJECT_TYPE", "PART_PREFIX", "PRELIM_MARK", "NAME", "PROFILE", "MATERIAL", "FINISH", "MAIN_PART", "DATE_MODIFY", "DRAWING_NAME", "PHASE_NAME", "PHASE", "USER_PHASE", "ASSEMBLY_PREFIX", "BOLT_FULL_NAME", "BOLT_SHORT_NAME", "BOLT_FULL_NAME2", "BOLT_STANDARD", "CONNECTED_PARTS", "CONNECTED_ASSEMBLIES", "WELD_TEXT", "WELD_TYPE1", "WELD_TYPE2", "TYPE", "TYPE1", "TYPE2", "ASSEMBLY_POSITION_CODE", "ASSEMBLY_BOTTOM_LEVEL_GLOBAL", "ASSEMBLY_TOP_LEVEL_GLOBAL", "BOTTOM_LEVEL_GLOBAL", "TOP_LEVEL_GLOBAL" };
        ArrayList dblProps = new ArrayList() { "LENGTH", "LENGTH_GROSS", "LENGTH_NET", "WEIGHT", "WEIGHT_NET", "DIAMETER", "COG_X", "COG_Y", "COG_Z", "HOLE_TOLERANCE", "DIAMETER_X", "DIAMETER_Y", "DIAMETER_X", "LOCATION_START_X", "LOCATION_START_Y", "LOCATION_START_Z", "LOCATION_END_X", "LOCATION_END_X", "LOCATION_END_X", "END1_ANGLE_Y", "END1_ANGLE_Z", "END2_ANGLE_Y", "END2_ANGLE_Z", "AREA", "BOLT_EDGE_DISTANCE", "BOLT_NPARTS", "VOLUME", "WELD_SIZE1", "WELD_SIZE2", "HEIGHT", "MESH_POS", "START_Y", "START_X", "START_Z", "END_Y", "END_X", "END_Z" };
        ArrayList intProps = new ArrayList() { "ID", "SERIAL_NUMBER", "MESH_POS", "ASSEMBLY.ID", "MODEL_TOTAL", "HAS_HOLES" };
        public List<string> changedObjectsDebugList = new List<string>();
        public bool checkParts;
        public bool checkBolts;
        public bool checkWelds;
        public bool checkCuts;  // and fittings
        public bool checkShowOnlyChangedIDsDetails;
        public bool checkShowOnlyChangedIDsList;
        public int decimalPrecision = 2;

        private void SetupThings()
        {
            this.Text = appName;
            this.Size = new Size(1000, 600);
        }

        public Form1()
        {
            InitializeComponent();
            SetupThings();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GetCurrentState();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            SaveCurrentState(objList, 1);
        }
        private void button5_Click(object sender, EventArgs e)
        {
            SaveCurrentState(objList, 2);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            slot1List = LoadState(1);
        }
        private void button6_Click(object sender, EventArgs e)
        {
            slot2List = LoadState(2);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            IDs_List IDL = new IDs_List(this);  // Pass the object to be operated with the new class:
            IDL.ShowIDs();
            IDL.MarkChangedFields();

            ObjectsTotal OBT = new ObjectsTotal(this);
            OBT.ShowObjectsBasedOnPartMark();
            OBT.MarkChangedFields();
        }

        /// <summary>
        /// Access to the GUI control from the outside
        /// </summary>
        public DataGridView _IDsListGrid
        {
            get { return IDsListGrid; }
            set { IDsListGrid = value; }
        }

        public DataGridView _IDsDetailsGrid
        {
            get { return IDsDetailsGrid; }
            set { IDsDetailsGrid = value; }
        }

        public DataGridView _ObjectsTotalGrid
        {
            get { return ObjectsTotalGrid; }
            set { ObjectsTotalGrid = value; }
        }

        //public GroupBox _groupFilters
        //{
        //    get { return _groupFilters; }
        //    set { _groupFilters = value; }
        //}

        private void GetCurrentState()
        {
            // Get Selected Objects:
            TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            objList = CreateObjectsHashtablesList(mos);
        }

        private void SaveCurrentState(List<Hashtable> currentStateListSorted, int slotNumber)
        {
            if (currentStateListSorted.Capacity > 0)
            {
                WriteToBinaryFile<List<Hashtable>>(userDesktopPath + "\\" + state + slotNumber + ".txt", currentStateListSorted);
            }
            else
            {
                MessageBox.Show(String.Format("Slot number {0} is empty. Nothing to save!", slotNumber));
            }
        }

        private List<Hashtable> LoadState(int slotNumber)
        {
            // Read From file:
            var stateXList = new List<Hashtable>();
            stateXList.Clear();
            return stateXList = ReadFromBinaryFile<List<Hashtable>>(userDesktopPath + "\\" + state + slotNumber + ".txt");
        }

        private List<Hashtable> CreateObjectsHashtablesList(TSMUI.ModelObjectSelector mos)
        {
            var objList = new List<Hashtable>();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();
            foreach (TSM.ModelObject o in moe)
            {
                if (new Type[] { typeof(TSM.Part), typeof(TSM.BoltGroup), typeof(TSM.Weld) } != null)
                {
                    Hashtable objHashtable = new Hashtable();
                    o.GetAllReportProperties(strProps, dblProps, intProps, ref objHashtable);
                    objList.Add(objHashtable);
                }
            }
            return objList;
        }

        /// <summary>
        /// Writes the given object instance to a binary file.
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the XML file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the XML file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file.
        /// </summary>
        /// <typeparam name="T">The type of object to read from the XML.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

        private void IDsListGrid_Click(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null) return;
            if (dgv.CurrentRow == null) return;
            if (dgv.CurrentRow.Selected)
            {
                IDs_Details IDD = new IDs_Details(this);    // Pass the object to be operated with the new class:
                IDD.ShowIDsDetails(dgv.CurrentRow.Cells[0].Value.ToString());
                IDD.MarkChangedFields();
            }
        }

        public void print(string p)
        {
            Console.WriteLine(p);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form_Activated(object sender, EventArgs e)
        {

        }

        private void chkParts_Paint(object sender, PaintEventArgs e)
        {
            checkParts = chkParts.Checked;
        }

        private void chkBolts_Paint(object sender, PaintEventArgs e)
        {
            checkBolts = chkBolts.Checked;
        }

        private void chkWelds_Paint(object sender, PaintEventArgs e)
        {
            checkWelds = chkWelds.Checked;
        }

        private void chkCuts_Paint(object sender, PaintEventArgs e)
        {
            checkCuts = chkCuts.Checked;
        }

        private void chkShowOnlyChangedDetails_Paint(object sender, PaintEventArgs e)
        {
            checkShowOnlyChangedIDsDetails = chkShowOnlyChangedDetails.Checked;
        }

        private void trackBarDecimalPrecision_Scroll(object sender, EventArgs e)
        {
            decimalPrecision = trackBarDecimalPrecision.Value;
            lblDecimalPrecision.Text = decimalPrecision.ToString();
        }

        private void chkShowOnlyChangeList_Paint(object sender, PaintEventArgs e)
        {
            checkShowOnlyChangedIDsList = chkShowOnlyChangeList.Checked;
        }
    }
}
