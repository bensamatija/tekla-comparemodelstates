﻿namespace NumberingChanges
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.IDsListGrid = new System.Windows.Forms.DataGridView();
            this.IDsDetailsGrid = new System.Windows.Forms.DataGridView();
            this.ObjectsTotalGrid = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkParts = new System.Windows.Forms.CheckBox();
            this.chkBolts = new System.Windows.Forms.CheckBox();
            this.chkWelds = new System.Windows.Forms.CheckBox();
            this.groupIDsListObjectsFilters = new System.Windows.Forms.GroupBox();
            this.chkCuts = new System.Windows.Forms.CheckBox();
            this.groupIDsDetailsFilters = new System.Windows.Forms.GroupBox();
            this.chkShowOnlyChangedDetails = new System.Windows.Forms.CheckBox();
            this.trackBarDecimalPrecision = new System.Windows.Forms.TrackBar();
            this.lblDecimalPrecision = new System.Windows.Forms.Label();
            this.groupBoxDecimaPrecisionValue = new System.Windows.Forms.GroupBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkShowOnlyChangeList = new System.Windows.Forms.CheckBox();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State1_PartMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State2_PartMark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDsListGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDsDetailsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectsTotalGrid)).BeginInit();
            this.groupIDsListObjectsFilters.SuspendLayout();
            this.groupIDsDetailsFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDecimalPrecision)).BeginInit();
            this.groupBoxDecimaPrecisionValue.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(195, 33);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 66);
            this.button1.TabIndex = 0;
            this.button1.Text = "Save State Slot 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(496, 33);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(158, 144);
            this.button2.TabIndex = 1;
            this.button2.Text = "Compare States";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(345, 33);
            this.button3.Margin = new System.Windows.Forms.Padding(6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(139, 66);
            this.button3.TabIndex = 2;
            this.button3.Text = "Load State Slot 1";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(25, 33);
            this.button4.Margin = new System.Windows.Forms.Padding(6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(158, 144);
            this.button4.TabIndex = 3;
            this.button4.Text = "Get State  from Selected";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(195, 111);
            this.button5.Margin = new System.Windows.Forms.Padding(6);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(138, 66);
            this.button5.TabIndex = 4;
            this.button5.Text = "Save State Slot 2";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(345, 111);
            this.button6.Margin = new System.Windows.Forms.Padding(6);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(139, 66);
            this.button6.TabIndex = 5;
            this.button6.Text = "Load State Slot 2";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            // 
            // IDsListGrid
            // 
            this.IDsListGrid.AllowUserToAddRows = false;
            this.IDsListGrid.AllowUserToDeleteRows = false;
            this.IDsListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.IDsListGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.State1_PartMark,
            this.State2_PartMark});
            this.IDsListGrid.Location = new System.Drawing.Point(25, 371);
            this.IDsListGrid.Name = "IDsListGrid";
            this.IDsListGrid.ReadOnly = true;
            this.IDsListGrid.RowHeadersVisible = false;
            this.IDsListGrid.RowTemplate.Height = 33;
            this.IDsListGrid.Size = new System.Drawing.Size(786, 671);
            this.IDsListGrid.TabIndex = 11;
            this.IDsListGrid.Click += new System.EventHandler(this.IDsListGrid_Click);
            // 
            // IDsDetailsGrid
            // 
            this.IDsDetailsGrid.AllowUserToAddRows = false;
            this.IDsDetailsGrid.AllowUserToDeleteRows = false;
            this.IDsDetailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.IDsDetailsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.IDsDetailsGrid.Location = new System.Drawing.Point(817, 371);
            this.IDsDetailsGrid.Name = "IDsDetailsGrid";
            this.IDsDetailsGrid.ReadOnly = true;
            this.IDsDetailsGrid.RowHeadersVisible = false;
            this.IDsDetailsGrid.RowTemplate.Height = 33;
            this.IDsDetailsGrid.Size = new System.Drawing.Size(630, 671);
            this.IDsDetailsGrid.TabIndex = 12;
            // 
            // ObjectsTotalGrid
            // 
            this.ObjectsTotalGrid.AllowUserToAddRows = false;
            this.ObjectsTotalGrid.AllowUserToDeleteRows = false;
            this.ObjectsTotalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ObjectsTotalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.ObjectsTotalGrid.Location = new System.Drawing.Point(1453, 371);
            this.ObjectsTotalGrid.Name = "ObjectsTotalGrid";
            this.ObjectsTotalGrid.ReadOnly = true;
            this.ObjectsTotalGrid.RowHeadersVisible = false;
            this.ObjectsTotalGrid.RowTemplate.Height = 33;
            this.ObjectsTotalGrid.Size = new System.Drawing.Size(482, 671);
            this.ObjectsTotalGrid.TabIndex = 13;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1160, 51);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(230, 31);
            this.textBox1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1020, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 25);
            this.label1.TabIndex = 15;
            this.label1.Text = "Search Filter";
            // 
            // chkParts
            // 
            this.chkParts.AutoSize = true;
            this.chkParts.Checked = true;
            this.chkParts.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkParts.Location = new System.Drawing.Point(6, 37);
            this.chkParts.Name = "chkParts";
            this.chkParts.Size = new System.Drawing.Size(94, 29);
            this.chkParts.TabIndex = 16;
            this.chkParts.Text = "Parts";
            this.chkParts.UseVisualStyleBackColor = true;
            this.chkParts.Paint += new System.Windows.Forms.PaintEventHandler(this.chkParts_Paint);
            // 
            // chkBolts
            // 
            this.chkBolts.AutoSize = true;
            this.chkBolts.Location = new System.Drawing.Point(6, 72);
            this.chkBolts.Name = "chkBolts";
            this.chkBolts.Size = new System.Drawing.Size(92, 29);
            this.chkBolts.TabIndex = 17;
            this.chkBolts.Text = "Bolts";
            this.chkBolts.UseVisualStyleBackColor = true;
            this.chkBolts.Paint += new System.Windows.Forms.PaintEventHandler(this.chkBolts_Paint);
            // 
            // chkWelds
            // 
            this.chkWelds.AutoSize = true;
            this.chkWelds.Location = new System.Drawing.Point(6, 107);
            this.chkWelds.Name = "chkWelds";
            this.chkWelds.Size = new System.Drawing.Size(104, 29);
            this.chkWelds.TabIndex = 18;
            this.chkWelds.Text = "Welds";
            this.chkWelds.UseVisualStyleBackColor = true;
            this.chkWelds.Paint += new System.Windows.Forms.PaintEventHandler(this.chkWelds_Paint);
            // 
            // groupIDsListObjectsFilters
            // 
            this.groupIDsListObjectsFilters.Controls.Add(this.chkCuts);
            this.groupIDsListObjectsFilters.Controls.Add(this.chkParts);
            this.groupIDsListObjectsFilters.Controls.Add(this.chkWelds);
            this.groupIDsListObjectsFilters.Controls.Add(this.chkBolts);
            this.groupIDsListObjectsFilters.Location = new System.Drawing.Point(25, 186);
            this.groupIDsListObjectsFilters.Name = "groupIDsListObjectsFilters";
            this.groupIDsListObjectsFilters.Size = new System.Drawing.Size(225, 179);
            this.groupIDsListObjectsFilters.TabIndex = 19;
            this.groupIDsListObjectsFilters.TabStop = false;
            this.groupIDsListObjectsFilters.Text = "Compare Change";
            // 
            // chkCuts
            // 
            this.chkCuts.AutoSize = true;
            this.chkCuts.Location = new System.Drawing.Point(6, 142);
            this.chkCuts.Name = "chkCuts";
            this.chkCuts.Size = new System.Drawing.Size(88, 29);
            this.chkCuts.TabIndex = 19;
            this.chkCuts.Text = "Cuts";
            this.chkCuts.UseVisualStyleBackColor = true;
            this.chkCuts.Paint += new System.Windows.Forms.PaintEventHandler(this.chkCuts_Paint);
            // 
            // groupIDsDetailsFilters
            // 
            this.groupIDsDetailsFilters.Controls.Add(this.groupBoxDecimaPrecisionValue);
            this.groupIDsDetailsFilters.Controls.Add(this.chkShowOnlyChangedDetails);
            this.groupIDsDetailsFilters.Location = new System.Drawing.Point(817, 143);
            this.groupIDsDetailsFilters.Name = "groupIDsDetailsFilters";
            this.groupIDsDetailsFilters.Size = new System.Drawing.Size(630, 222);
            this.groupIDsDetailsFilters.TabIndex = 20;
            this.groupIDsDetailsFilters.TabStop = false;
            this.groupIDsDetailsFilters.Text = "Compare Filters";
            // 
            // chkShowOnlyChangedDetails
            // 
            this.chkShowOnlyChangedDetails.AutoSize = true;
            this.chkShowOnlyChangedDetails.Location = new System.Drawing.Point(6, 37);
            this.chkShowOnlyChangedDetails.Name = "chkShowOnlyChangedDetails";
            this.chkShowOnlyChangedDetails.Size = new System.Drawing.Size(232, 29);
            this.chkShowOnlyChangedDetails.TabIndex = 17;
            this.chkShowOnlyChangedDetails.Text = "Show only changed";
            this.chkShowOnlyChangedDetails.UseVisualStyleBackColor = true;
            this.chkShowOnlyChangedDetails.Paint += new System.Windows.Forms.PaintEventHandler(this.chkShowOnlyChangedDetails_Paint);
            // 
            // trackBarDecimalPrecision
            // 
            this.trackBarDecimalPrecision.LargeChange = 1;
            this.trackBarDecimalPrecision.Location = new System.Drawing.Point(37, 37);
            this.trackBarDecimalPrecision.Maximum = 6;
            this.trackBarDecimalPrecision.Name = "trackBarDecimalPrecision";
            this.trackBarDecimalPrecision.Size = new System.Drawing.Size(207, 90);
            this.trackBarDecimalPrecision.TabIndex = 18;
            this.trackBarDecimalPrecision.Value = 2;
            this.trackBarDecimalPrecision.Scroll += new System.EventHandler(this.trackBarDecimalPrecision_Scroll);
            // 
            // lblDecimalPrecision
            // 
            this.lblDecimalPrecision.AutoSize = true;
            this.lblDecimalPrecision.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDecimalPrecision.Location = new System.Drawing.Point(6, 37);
            this.lblDecimalPrecision.Name = "lblDecimalPrecision";
            this.lblDecimalPrecision.Size = new System.Drawing.Size(25, 28);
            this.lblDecimalPrecision.TabIndex = 19;
            this.lblDecimalPrecision.Text = "2";
            // 
            // groupBoxDecimaPrecisionValue
            // 
            this.groupBoxDecimaPrecisionValue.Controls.Add(this.trackBarDecimalPrecision);
            this.groupBoxDecimaPrecisionValue.Controls.Add(this.lblDecimalPrecision);
            this.groupBoxDecimaPrecisionValue.Location = new System.Drawing.Point(6, 80);
            this.groupBoxDecimaPrecisionValue.Name = "groupBoxDecimaPrecisionValue";
            this.groupBoxDecimaPrecisionValue.Size = new System.Drawing.Size(270, 130);
            this.groupBoxDecimaPrecisionValue.TabIndex = 21;
            this.groupBoxDecimaPrecisionValue.TabStop = false;
            this.groupBoxDecimaPrecisionValue.Text = "Decimal precision value";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Descriptor";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "State 1";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 70;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "State 2";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 70;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkShowOnlyChangeList);
            this.groupBox1.Location = new System.Drawing.Point(256, 186);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(555, 179);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Compare Filters";
            // 
            // chkShowOnlyChangeList
            // 
            this.chkShowOnlyChangeList.AutoSize = true;
            this.chkShowOnlyChangeList.Location = new System.Drawing.Point(6, 37);
            this.chkShowOnlyChangeList.Name = "chkShowOnlyChangeList";
            this.chkShowOnlyChangeList.Size = new System.Drawing.Size(232, 29);
            this.chkShowOnlyChangeList.TabIndex = 17;
            this.chkShowOnlyChangeList.Text = "Show only changed";
            this.chkShowOnlyChangeList.UseVisualStyleBackColor = true;
            this.chkShowOnlyChangeList.Paint += new System.Windows.Forms.PaintEventHandler(this.chkShowOnlyChangeList_Paint);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 50;
            // 
            // State1_PartMark
            // 
            this.State1_PartMark.HeaderText = "State 1 - Part Mark";
            this.State1_PartMark.Name = "State1_PartMark";
            this.State1_PartMark.ReadOnly = true;
            this.State1_PartMark.Width = 160;
            // 
            // State2_PartMark
            // 
            this.State2_PartMark.HeaderText = "State 2 - Part Mark";
            this.State2_PartMark.Name = "State2_PartMark";
            this.State2_PartMark.ReadOnly = true;
            this.State2_PartMark.Width = 160;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Part Mark";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 50;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "State 1 Q";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "State 2 Q";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 70;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1972, 1060);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupIDsDetailsFilters);
            this.Controls.Add(this.groupIDsListObjectsFilters);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.ObjectsTotalGrid);
            this.Controls.Add(this.IDsDetailsGrid);
            this.Controls.Add(this.IDsListGrid);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Location = new System.Drawing.Point(2000, 0);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.Form_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDsListGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDsDetailsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectsTotalGrid)).EndInit();
            this.groupIDsListObjectsFilters.ResumeLayout(false);
            this.groupIDsListObjectsFilters.PerformLayout();
            this.groupIDsDetailsFilters.ResumeLayout(false);
            this.groupIDsDetailsFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDecimalPrecision)).EndInit();
            this.groupBoxDecimaPrecisionValue.ResumeLayout(false);
            this.groupBoxDecimaPrecisionValue.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Data.DataSet dataSet1;
        private System.Windows.Forms.DataGridView IDsListGrid;
        private System.Windows.Forms.DataGridView IDsDetailsGrid;
        private System.Windows.Forms.DataGridView ObjectsTotalGrid;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkParts;
        private System.Windows.Forms.CheckBox chkBolts;
        private System.Windows.Forms.CheckBox chkWelds;
        private System.Windows.Forms.GroupBox groupIDsListObjectsFilters;
        private System.Windows.Forms.CheckBox chkCuts;
        private System.Windows.Forms.GroupBox groupIDsDetailsFilters;
        private System.Windows.Forms.CheckBox chkShowOnlyChangedDetails;
        private System.Windows.Forms.TrackBar trackBarDecimalPrecision;
        private System.Windows.Forms.Label lblDecimalPrecision;
        private System.Windows.Forms.GroupBox groupBoxDecimaPrecisionValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkShowOnlyChangeList;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn State1_PartMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn State2_PartMark;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}

