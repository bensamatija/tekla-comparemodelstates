﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.Runtime.Remoting;
using Tekla.Structures.Internal;
using System.Diagnostics;

using System.Runtime.Serialization;
using System.Xml.Serialization;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using System.IO;
//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

using System.Collections;

//using NumberingChanges;


namespace NumberingChanges
{
    class IDs_List
    {
        //List<Hashtable> StatexListSorted = new List<Hashtable>();

        // Create communication with the existing object in another class:
        private Form1 mainForm;
        public IDs_List(Form1 f)
        {
            this.mainForm = f;
        }

        /// <summary>
        /// Visual Properties for the Data Grid:
        /// </summary>
        private void VisualPropertiesReset()
        {
            mainForm._IDsListGrid.Rows.Clear();
            mainForm._IDsListGrid.RowTemplate.Height = 15;
            mainForm._IDsListGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            mainForm._IDsListGrid.Columns[0].Width = 50;
            mainForm._IDsListGrid.Columns[1].Width = 160;    //65
            mainForm._IDsListGrid.Columns[2].Width = 160;
            mainForm._IDsListGrid.AllowUserToResizeRows = false;
        }

        public void ShowIDs()
        {

            VisualPropertiesReset();
            int row = 0;

            // Check if the State is loaded into the Slot:
            if (mainForm.slot1List.Count > 0 && mainForm.slot2List.Count > 0)
            {
                // Get every object:             
                foreach (Hashtable obj1 in mainForm.slot1List)  //for (int i = 0; i <= State1List.Count; i++) //
                {
                    // Get every object:                   
                    foreach (Hashtable obj2 in mainForm.slot2List)  //for (int j = 0; j <= State1List.Count; j++) //
                    {
                        try
                        {
                            if (obj1.Count != 0 && obj2.Count != 0)
                            {
                                // Get the object with same ID:
                                if (obj1["ID"].ToString() == obj2["ID"].ToString())
                                {
                                    if ((mainForm.checkParts == true) && (obj1["OBJECT_TYPE"].ToString() == "PART"))
                                    {
                                        mainForm._IDsListGrid.Rows.Add();
                                        //AddDataToTheTable(mainForm._IDsListGrid, row, 0, obj1, "ID");
                                        //AddDataToTheTable(mainForm._IDsListGrid, row, 1, obj1, "PART_POS");
                                        //AddDataToTheTable(mainForm._IDsListGrid, row, 2, obj2, "PART_POS");
                                        mainForm._IDsListGrid.Rows[row].Cells[0].Value = obj1["ID"].ToString();
                                        mainForm._IDsListGrid.Rows[row].Cells[1].Value = obj1["PART_POS"].ToString();
                                        mainForm._IDsListGrid.Rows[row].Cells[2].Value = obj2["PART_POS"].ToString();
                                        row++;
                                        break;
                                    }
                                    else if ((mainForm.checkBolts == true) && (obj1["OBJECT_TYPE"].ToString() == "SCREW"))
                                    {
                                        mainForm._IDsListGrid.Rows.Add();
                                        mainForm._IDsListGrid.Rows[row].Cells[0].Value = obj1["ID"].ToString();
                                        mainForm._IDsListGrid.Rows[row].Cells[1].Value = obj1["NAME"].ToString();
                                        mainForm._IDsListGrid.Rows[row].Cells[2].Value = obj2["NAME"].ToString();
                                        row++;
                                        break;
                                    }
                                    else if ((mainForm.checkWelds == true) && (obj1["OBJECT_TYPE"].ToString() == "WELDING"))
                                    {
                                        mainForm._IDsListGrid.Rows.Add();
                                        mainForm._IDsListGrid.Rows[row].Cells[0].Value = obj1["ID"].ToString();
                                        mainForm._IDsListGrid.Rows[row].Cells[1].Value = obj1["OBJECT_TYPE"].ToString() + " " + obj1["WELD_SIZE1"].ToString() + "/" + obj1["WELD_SIZE2"].ToString();
                                        mainForm._IDsListGrid.Rows[row].Cells[2].Value = obj2["OBJECT_TYPE"].ToString() + " " + obj2["WELD_SIZE1"].ToString() + "/" + obj2["WELD_SIZE2"].ToString();
                                        row++;
                                        break;
                                    }
                                    else if ((mainForm.checkCuts == true) && (obj1["OBJECT_TYPE"].ToString() == "CUT"))
                                    {
                                        mainForm._IDsListGrid.Rows.Add();
                                        mainForm._IDsListGrid.Rows[row].Cells[0].Value = obj1["ID"].ToString();
                                        row++;
                                        break;
                                    }
                                    else if ((mainForm.checkCuts == true) && (obj1["OBJECT_TYPE"].ToString() == "FITTING"))
                                    {
                                        mainForm._IDsListGrid.Rows.Add();
                                        mainForm._IDsListGrid.Rows[row].Cells[0].Value = obj1["ID"].ToString();
                                        row++;
                                        break;
                                    }
                                    //break;
                                }
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.ToString()); }
                    }
                }
                // Sort the list:
                mainForm._IDsListGrid.Sort(this.mainForm._IDsListGrid.Columns[1], ListSortDirection.Ascending);
            }
            else
            {
                MessageBox.Show(String.Format("Nothing to compare. Load State Slots first!"));
            }
        }

        //static private void AddDataToTheTable(DataGridView dataGrid, int row, int cell, Hashtable obj, string attribute)
        //{
        //    dataGrid.Rows[row].Cells[cell].Value = obj[attribute].ToString();
        //}

        /// <summary>
        /// Mark the table values with Colors
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        public void MarkChangedFields()
        {
            try
            {
                for (int row = 0; row < mainForm._IDsListGrid.Rows.Count; row++)
                {
                    if (mainForm._IDsListGrid.Rows[row].Cells[1].Value.ToString() == mainForm._IDsListGrid.Rows[row].Cells[2].Value.ToString())
                    {
                        // MATCH:
                        if (mainForm.checkShowOnlyChangedIDsList == true)
                        {
                            DataGridViewRow thisRow = mainForm._IDsListGrid.Rows[row];
                            mainForm._IDsListGrid.Rows.RemoveAt(row);  //thisRow.Index
                            row--;
                        }
                    }
                    else
                    {
                        // DIFFERENCE:
                        mainForm._IDsListGrid.Rows[row].Cells[1].Style.BackColor = Color.Red;
                        mainForm._IDsListGrid.Rows[row].Cells[2].Style.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
