﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.Runtime.Remoting;
using Tekla.Structures.Internal;
using System.Diagnostics;

using System.Runtime.Serialization;
using System.Xml.Serialization;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using System.IO;
//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

using System.Collections;


namespace NumberingChanges
{
    class ObjectsTotal
    {
        // Create communication with the existing object in another class:
        private Form1 mainForm;
        public ObjectsTotal(Form1 f)
        {
            this.mainForm = f;
        }

        /// <summary>
        /// Visual Properties for the Data Grid:
        /// </summary>
        private void VisualPropertiesReset()
        {
            mainForm._ObjectsTotalGrid.Rows.Clear();
            mainForm._ObjectsTotalGrid.RowTemplate.Height = 15;
            //mainForm._ObjectsTotalGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            mainForm._ObjectsTotalGrid.Columns[0].Width = 70;
            mainForm._ObjectsTotalGrid.Columns[1].Width = 70;
            mainForm._ObjectsTotalGrid.Columns[2].Width = 70;
            mainForm._ObjectsTotalGrid.AllowUserToResizeRows = false;
        }

        public void ShowObjectsBasedOnPartMark()
        {
            VisualPropertiesReset();
            int row = 0;
            bool needMatch = false;
            List<string> partPositions = new List<string>();
            List<string> partPositionsAddition = new List<string>();
            // Check if the State is loaded into the Slot:
            if (mainForm.slot1List.Count > 0 && mainForm.slot2List.Count > 0)
            {
                foreach (Hashtable obj1 in mainForm.slot1List)
                {
                    foreach (Hashtable obj2 in mainForm.slot2List)
                    {
                        needMatch = true;
                        try
                        {
                            if (obj1.Count != 0 && obj2.Count != 0)
                            {
                                if (obj1["OBJECT_TYPE"].ToString() == "PART" && obj2["OBJECT_TYPE"].ToString() == "PART")
                                {
                                    if (obj1["PART_POS"].ToString() == obj2["PART_POS"].ToString())
                                    {
                                        needMatch = false;
                                        mainForm._ObjectsTotalGrid.Rows.Add();
                                        mainForm._ObjectsTotalGrid.Rows[row].Cells[0].Value = obj1["PART_POS"].ToString();
                                        mainForm._ObjectsTotalGrid.Rows[row].Cells[1].Value = obj1["MODEL_TOTAL"].ToString();
                                        mainForm._ObjectsTotalGrid.Rows[row].Cells[2].Value = obj2["MODEL_TOTAL"].ToString();
                                        partPositions.Add(obj1["PART_POS"].ToString());
                                        row++;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.ToString()); }
                    }
                    // 
                    if (needMatch == true)
                    {
                        if (obj1.Count != 0)
                        {
                            if (obj1["OBJECT_TYPE"].ToString() == "PART")
                            {
                                needMatch = true;
                                mainForm._ObjectsTotalGrid.Rows.Add();
                                mainForm._ObjectsTotalGrid.Rows[row].Cells[0].Value = obj1["PART_POS"].ToString();
                                mainForm._ObjectsTotalGrid.Rows[row].Cells[1].Value = obj1["MODEL_TOTAL"].ToString();
                                mainForm._ObjectsTotalGrid.Rows[row].Cells[2].Value = "DELETED";
                                partPositions.Add(obj1["PART_POS"].ToString());
                                row++;
                            }
                        }
                    }
                }
                // PLACE FOR OPTIMIZATION ?

                foreach (Hashtable obj2 in mainForm.slot1List)
                {
                    //foreach (Hashtable obj1 in mainForm.slot1List)
                    //{
                    //    needMatch = true;
                    //    try
                    //    {
                    //        if (obj1.Count != 0 && obj2.Count != 0)
                    //        {
                    //            if (obj1["OBJECT_TYPE"].ToString() == "PART" && obj2["OBJECT_TYPE"].ToString() == "PART")
                    //            {
                    //                if (obj1["PART_POS"].ToString() == obj2["PART_POS"].ToString())
                    //                {
                    //                    needMatch = false;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    catch (Exception ex) { MessageBox.Show(ex.ToString()); }
                    //}

                    if (needMatch == true)
                    {
                        if (obj2.Count != 0)
                        {
                            if (obj2["OBJECT_TYPE"].ToString() == "PART")
                            {
                                foreach (string pp in partPositions)
                                {
                                    if (obj2["PART_POS"].ToString() != pp)
                                    {
                                        needMatch = true;
                                        mainForm._ObjectsTotalGrid.Rows.Add();
                                        mainForm._ObjectsTotalGrid.Rows[row].Cells[0].Value = obj2["PART_POS"].ToString();
                                        mainForm._ObjectsTotalGrid.Rows[row].Cells[2].Value = obj2["MODEL_TOTAL"].ToString();
                                        mainForm._ObjectsTotalGrid.Rows[row].Cells[1].Value = "NEW";
                                        partPositionsAddition.Add(obj2["PART_POS"].ToString());
                                        row++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Mark the table values with Colors
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        public void MarkChangedFields()
        {
            try
            {
                for (int row = 0; row < mainForm._ObjectsTotalGrid.Rows.Count; row++)
                {
                    if (mainForm._ObjectsTotalGrid.Rows[row].Cells[1].Value.ToString() == mainForm._ObjectsTotalGrid.Rows[row].Cells[2].Value.ToString())
                    {
                        // MATCH:
                        //if (mainForm.checkShowOnlyChangedIDsDetails == true)
                        {
                            DataGridViewRow thisRow = mainForm._ObjectsTotalGrid.Rows[row];
                            mainForm._ObjectsTotalGrid.Rows.RemoveAt(row);  //thisRow.Index
                            row--;
                        }
                    }
                    else
                    {
                        // DIFFERENCE:
                        mainForm._ObjectsTotalGrid.Rows[row].Cells[1].Style.BackColor = Color.Red;
                        mainForm._ObjectsTotalGrid.Rows[row].Cells[2].Style.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception) { }
        }
    }
}
