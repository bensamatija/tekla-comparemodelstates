﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using System.IO;
using System.Collections;

namespace _N_CustomObject
{
    [Serializable]
    public class CustomObject
    {
        public CustomObject() { }

        public CustomObject(string id, string guid, double main_Part, double model_Total, string profile, string name, string material, double length, double length_Gross, double assemblyPositionNumber, double cog_X, double cog_Y, double cog_Z, string date_modify, double start_X, double start_Y, double start_Z, double end_X, double end_Y, double end_Z, double end1_Angle_Y, double end1_Angle_Z, double end2_Angle_Y, double end2_Angle_Z, double hole_Has, string drawingName, string part_Prefix, double part_PositionNumber, double profile_Weight, double profile_WeightNet, string phase_Name, string phase_UserPhase, double screw_Hole_Diameter_X, double screw_Hole_Diameter_Y, double volume, double weight, double serial_Number, double hole_Tolerance, string partMark)
        {
            id = ID;
            guid = GUID;
            main_Part = MAIN_PART;
            model_Total = MODEL_TOTAL;
            profile = PROFILE;
            name = NAME;
            material = MATERIAL;
            length = LENGTH;
            length_Gross = LENGTH_GROSS;
            assemblyPositionNumber = ASSEMBLYPOSITIONNUMBERNUMBER;
            cog_X = COG_X;
            cog_Y = COG_Y;
            cog_Z = COG_Z;
            date_modify = DATE_MODIFY;
            start_X = LOCATION_START_X;
            start_Y = LOCATION_START_Y;
            start_Z = LOCATION_START_Z;
            end_X = LOCATION_END_X;
            end_Y = LOCATION_END_Y;
            end_Z = LOCATION_END_Z;
            end1_Angle_Y = END1_ANGLE_Y;
            end1_Angle_Z = END1_ANGLE_Z;
            end2_Angle_Y = END2_ANGLE_Y;
            end2_Angle_Z = END2_ANGLE_Z;
            hole_Has = HAS_HOLES;
            drawingName = DRAWING_NAME;
            partMark = PartMark;
            part_Prefix = PART_PREFIX;
            part_PositionNumber = PART_POSITIONNUMBER;
            profile_Weight = PROFILE_WEIGHT;
            profile_WeightNet = PROFILE_WEIGHTNET;
            phase_Name = PHASE_NAME;
            phase_UserPhase = PHASE_USERPHASE;
            screw_Hole_Diameter_X = SCREW_HOLE_DIAMETER_X;
            screw_Hole_Diameter_Y = SCREW_HOLE_DIAMETER_Y;
            volume = VOLUME;
            weight = WEIGHT;
            serial_Number = SERIAL_NUMBER;
            hole_Tolerance = HOLE_TOLERANCE;
        }

        // Properties:
        //public TS.Identifier Id { get; set; }
        //public TS.Identifier Guid { get; set; }
        public string ID { get; set; }
        public string GUID { get; set; }
        public double MAIN_PART { get; set; }
        public double MODEL_TOTAL { get; set; }
        public string PROFILE { get; set; }
        public string NAME { get; set; }
        public string MATERIAL { get; set; }
        public double LENGTH { get; set; }
        public double LENGTH_GROSS { get; set; }
        public double ASSEMBLYPOSITIONNUMBERNUMBER { get; set; }
        public double COG_X { get; set; }
        public double COG_Y { get; set; }
        public double COG_Z { get; set; }
        public string DATE_MODIFY { get; set; }
        public double LOCATION_START_X { get; set; }
        public double LOCATION_START_Y { get; set; }
        public double LOCATION_START_Z { get; set; }
        public double LOCATION_END_X { get; set; }
        public double LOCATION_END_Y { get; set; }
        public double LOCATION_END_Z { get; set; }
        public double END1_ANGLE_Y { get; set; }
        public double END1_ANGLE_Z { get; set; }
        public double END2_ANGLE_Y { get; set; }
        public double END2_ANGLE_Z { get; set; }
        public double HAS_HOLES { get; set; }
        public string DRAWING_NAME { get; set; }
        public string PartMark { get; set; }
        public string PART_PREFIX { get; set; }
        public double PART_POSITIONNUMBER { get; set; }
        public double PROFILE_WEIGHT { get; set; }
        public double PROFILE_WEIGHTNET { get; set; }
        public string PHASE_NAME { get; set; }
        public string PHASE_USERPHASE { get; set; }
        public double SCREW_HOLE_DIAMETER_X { get; set; }
        public double SCREW_HOLE_DIAMETER_Y { get; set; }
        public double VOLUME { get; set; }
        public double WEIGHT { get; set; }
        public double SERIAL_NUMBER { get; set; }
        public double HOLE_TOLERANCE { get; set; }
    }

    //[Serializable]
    public class TeklaTemplateAttributes
    {
        //public CC() { }
        //public ArrayList CC(Collection Enum);

        //List<TeklaTemplateAttributes> aaa = new List<TeklaTemplateAttributes>();

        //TemplatesCollection t = (TemplatesCollection)3;

        public enum TemplatesCollection
        {
            WEIGHT,
            VOLUME
        }

        //Collection val = Collection.WEIGHT;
    }
}
