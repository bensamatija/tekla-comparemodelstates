﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Globalization;
using System.Runtime.Remoting;
using Tekla.Structures.Internal;
using System.Diagnostics;

using System.Runtime.Serialization;
using System.Xml.Serialization;

//Tekla:
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using System.IO;
//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;

using System.Collections;

//using NumberingChanges;


namespace NumberingChanges
{
    class IDs_Details
    {
        // Create communication with the existing object in another class:
        private Form1 mainForm;
        public IDs_Details(Form1 f)
        {
            this.mainForm = f;
        }

        /// <summary>
        /// Visual Properties for the Data Grid:
        /// </summary>
        private void VisualPropertiesReset()
        {
            mainForm._IDsDetailsGrid.Rows.Clear();
            mainForm._IDsDetailsGrid.RowTemplate.Height = 15;
            //mainForm._IDsDetailsGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            mainForm._IDsDetailsGrid.Columns[0].Width = 150;
            mainForm._IDsDetailsGrid.Columns[1].Width = 70;
            mainForm._IDsDetailsGrid.Columns[2].Width = 70;
            mainForm._IDsDetailsGrid.AllowUserToResizeRows = false;
        }

        public void ShowIDsDetails(string ID)
        {
            VisualPropertiesReset();
            int decimalPrecision = mainForm.decimalPrecision;
            int row = 0;
            try
            {
                // Get hashtable by ID and show Details:
                if (mainForm.slot1List.Count > 0 && mainForm.slot2List.Count > 0)
                {
                    foreach (Hashtable obj1 in mainForm.slot1List)
                    {
                        try
                        {
                            if (obj1.Count != 0)
                            {
                                if (obj1["ID"].ToString() == ID)
                                {
                                    foreach (Hashtable obj2 in mainForm.slot2List)
                                    {
                                        if (obj2.Count != 0)
                                        {
                                            if (obj2["ID"].ToString() == ID)
                                            {
                                                // Write IDs, obj1's and obj2's Details in each row:
                                                foreach (DictionaryEntry d1 in obj1)
                                                {
                                                    mainForm._IDsDetailsGrid.Rows.Add();
                                                    mainForm._IDsDetailsGrid.Rows[row].Cells[0].Value = d1.Key.ToString();
                                                    //mainForm._IDsDetailsGrid.Rows[row].Cells[1].Value = d1.Value.ToString();
                                                    mainForm._IDsDetailsGrid.Rows[row].Cells[1].Value = TryFloatRounding(d1.Value.ToString(), decimalPrecision);

                                                    row++;
                                                }
                                                row = 0;
                                                foreach (DictionaryEntry d2 in obj2)
                                                {
                                                    //mainForm._IDsDetailsGrid.Rows[row].Cells[2].Value = d2.Value.ToString();
                                                    mainForm._IDsDetailsGrid.Rows[row].Cells[2].Value = TryFloatRounding(d2.Value.ToString(), decimalPrecision);
                                                    row++;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception) { }
                    }
                    // Sort the list:
                    mainForm._IDsDetailsGrid.Sort(this.mainForm._IDsDetailsGrid.Columns[0], ListSortDirection.Ascending);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        /// <summary>
        /// Mark the table values with Colors
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        public void MarkChangedFields()
        {
            try
            {
                for (int row = 0; row < mainForm._IDsDetailsGrid.Rows.Count; row++)
                {
                    if (mainForm._IDsDetailsGrid.Rows[row].Cells[1].Value.ToString() == mainForm._IDsDetailsGrid.Rows[row].Cells[2].Value.ToString())
                    {
                        // MATCH:
                        if (mainForm.checkShowOnlyChangedIDsDetails == true)
                        {
                            DataGridViewRow thisRow = mainForm._IDsDetailsGrid.Rows[row];
                            mainForm._IDsDetailsGrid.Rows.RemoveAt(row);  //thisRow.Index
                            row--;
                        }
                    }
                    else
                    {
                        // DIFFERENCE:
                        mainForm._IDsDetailsGrid.Rows[row].Cells[1].Style.BackColor = Color.Red;
                        mainForm._IDsDetailsGrid.Rows[row].Cells[2].Style.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception) { }
        }

        //                  REWRITE THIS, IT IS CAUSING OVERHEAD!!!
        /// <summary>
        /// Tries to round the float, sent as a string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="precision"></param>
        /// <returns></returns>
        private string TryFloatRounding(string value, int precision)
        {
            string str;
            try
            {

                if (value != null)
                {
                    if (value != string.Empty)
                    {
                        //if (IsDigitsOnly(value))
                        {
                            // Check if this is numeric number:
                            value = value.Replace(",", ".");
                            double flt = double.Parse(value, CultureInfo.InvariantCulture.NumberFormat);
                            double dbl = Math.Round(flt, precision, MidpointRounding.AwayFromZero);
                            //str = Math.Truncate()
                            //f = (float)value;
                            return dbl.ToString();
                        }
                        return value;
                    }
                    return value;
                }
                return value;
            }
            catch (FormatException)
            {
                // If it is not numeric it must be Characters:
                return value;
            }
            return value;
        }

        static bool DigitsOnly(string s)
        {
            int len = s.Length;
            for (int i = 0; i < len; ++i)
            {
                char c = s[i];
                if (c >= '0' || c <= '9' || c == '-' || c == '+' || c == '.' || c == ' ' || c == ',')
                    return false;
            }
            return true;
        }

        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if ((c >= '0' || c <= '9') || c == '-' || c == '+' || c == '.' || c == ' ' || c == ',')
                    return false;
            }
            return true;
        }
    }
}
